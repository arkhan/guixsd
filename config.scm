;; This is an operating system configuration generated
;; by the graphical installer.
(use-modules (gnu)
             (gnu packages cups)
             (gnu system nss)
             (guix build-system python)
             (guix download)
             (guix packages)
             (guix licenses)
             (nongnu packages linux))
(use-service-modules admin avahi base dbus cups desktop networking ssh xorg sddm docker sysctl)
(use-package-modules admin avahi bootloaders certs cups disk fonts file libusb version-control
                     ssh tls wm xdisorg xorg shells python python-xyz)

(define-public xonsh
  (package
    (name "xonsh")
    (version "0.9.18")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "xonsh" version))
        (sha256
          (base32
            "1h4rrwzwiwkyi9p49sjn97rl39fqq2r23hchzsw0s3fcwa7m8fkj"))
        (modules '((guix build utils)))
        (snippet
         `(begin
            ;; Delete bundled PLY.
            (delete-file-recursively "xonsh/ply")
            (substitute* "setup.py"
              (("\"xonsh\\.ply\\.ply\",") ""))
            ;; Use our properly packaged PLY instead.
            (substitute* (list "setup.py"
                               "tests/test_lexer.py"
                               "xonsh/__amalgam__.py"
                               "xonsh/lexer.py"
                               "xonsh/parsers/base.py"
                               "xonsh/xonfig.py")
              (("from xonsh\\.ply\\.(.*) import" _ module)
               (format #f "from ~a import" module))
              (("from xonsh\\.ply import") "import"))
            #t))))
    (build-system python-build-system)
    (arguments
     '(;; TODO Try running run the test suite.
       ;; See 'requirements-tests.txt' in the source distribution for more
       ;; information.
       #:tests? #f))
    (inputs
     `(("python-ply" ,python-ply)
       ("python-prompt-toolkit@2.0.7" ,python-prompt-toolkit)))
    (home-page "https://xon.sh/")
    (synopsis "Python-ish shell")
    (description
     "Xonsh is a Python-ish, BASHwards-looking shell language and command
prompt.  The language is a superset of Python 3.4+ with additional shell
primitives that you are used to from Bash and IPython.  It works on all major
systems including Linux, Mac OSX, and Windows.  Xonsh is meant for the daily
use of experts and novices alike.")
    (license bsd-2)))

(operating-system
 (kernel linux)
 (firmware
  (list linux-firmware))
 (locale "es_EC.utf8")
 (timezone "America/Guayaquil")

 (keyboard-layout
  (keyboard-layout "us" "altgr-intl"))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (target "/boot/efi")
   (keyboard-layout keyboard-layout)))

 (kernel-arguments
  '("acpi_osi=!"
    "acpi_osi=\"Windows 2009\""))

 (swap-devices (list "/dev/sda2"))

 (file-systems
  (cons* (file-system
          (mount-point "/boot/efi")
          (device (uuid "E6F6-D047" 'fat32))
          (type "vfat"))
         (file-system
          (mount-point "/")
          (device
           (uuid "62601caa-15c7-4b96-987e-7be31684ebf6"
                 'ext4))
          (type "ext4"))
         (file-system
          (mount-point "/media/data")
          (device
           (uuid "8f9ecdf2-fa43-4330-9785-594d2ad0ad4f"
                 'ext4))
          (type "ext4"))
         %base-file-systems))

 (host-name "Pantheon")

 (users (cons*
         (user-account
          (name "arkhan")
          (comment "arkhan")
          (group "users")
          (home-directory "/home/arkhan")
          (shell #~(string-append #$xonsh "/bin/xonsh"))
          (supplementary-groups
           '("wheel" "netdev" "audio" "video" "docker")))
         %base-user-accounts))

  (packages
   (append
    (map specification->package
         '("avahi"
           "bspwm"
           "sxhkd"
           "polybar"
           "rofi"
           "rxvt-unicode"
           "rpcbind"
           "flatpak"
           "nss-certs"
           "bluez"
           "docker"
           "docker-compose"))
    %base-packages))

  (services
   (append
    (list (service sddm-service-type
                   (sddm-configuration
                    (xorg-configuration
                     (xorg-configuration
                      (keyboard-layout keyboard-layout)))))
          (service sysctl-service-type
                     (sysctl-configuration
                      (settings '(("net.ipv4.ip_forward" . "1")
                                  ("fs.inotify.max_user_watches" . "524288")))))
          (service cups-service-type
                   (cups-configuration
                    (server-name host-name)
                    (host-name-lookups #t)
                    (web-interface? #t)
                    (browsing? #t)
                    (default-paper-size "a4")
                    (extensions
                     (list cups-filters escpr hplip-minimal))))
          (service network-manager-service-type)
          (service wpa-supplicant-service-type)
          (service upower-service-type)
          (service polkit-service-type)
          (service elogind-service-type)
          (service docker-service-type)
          (dbus-service)
          (bluetooth-service #:auto-enable? #t))
    %base-services)))
