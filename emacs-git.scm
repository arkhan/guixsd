(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix build-system gnu)
             (guix build-system glib-or-gtk)
             (gnu packages)
             (gnu packages acl)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages compression)
             (gnu packages emacs)
             (gnu packages fontutils)
             (gnu packages fribidi)
             (gnu packages gd)
             (gnu packages gettext)
             (gnu packages glib)
             (gnu packages gnome)     ; for librsvg
             (gnu packages gtk)
             (gnu packages guile)
             (gnu packages image)
             (gnu packages imagemagick)
             (gnu packages linux)     ; alsa-lib
             (gnu packages mail)      ; for mailutils
             (gnu packages ncurses)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages tls)
             (gnu packages web)       ; for jansson
             (gnu packages webkit)
             (gnu packages xml)
             (gnu packages xorg)
             (guix utils)
             (srfi srfi-1))

(let ((commit "659ed857c04936140fea847795f8b85c5dcc3920")
      (revision "0")
      (emacs-version "28.0.50"))
  (package
   (inherit emacs)
   (name "emacs-git")
   (version (git-version emacs-version revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://git.savannah.gnu.org/git/emacs.git")
           (commit commit)))
     (sha256
      (base32 "0f7d003jh45mpk6cjd2xwz2i4wwjxsdh1p6p0xkx8c7ihxwchbpx"))
     (file-name (git-file-name name version))
     (patches (search-patches "emacs-fix-scheme-indent-function.patch"
                              "emacs-source-date-epoch.patch"))
     (modules (origin-modules (package-source emacs)))
     ;; TODO: once the snippet for `emacs' is changed to not fail when
     ;; eshell/esh-groups.el does not exist, replace this snippet with
     ;; (snippet (origin-snippet (package-source emacs))))).
     (snippet
      '(with-directory-excursion "lisp"
        ;; Make sure Tramp looks for binaries in the right places on
        ;; remote Guix System machines, where 'getconf PATH' returns
        ;; something bogus.
                                 (substitute* "net/tramp-sh.el"
                                              ;; Patch the line after "(defcustom tramp-remote-path".
                                              (("\\(tramp-default-remote-path")
                (format #f "(tramp-default-remote-path ~s ~s ~s ~s "
                        "~/.guix-profile/bin" "~/.guix-profile/sbin"
                        "/run/current-system/profile/bin"
                        "/run/current-system/profile/sbin")))

             ;; Make sure Man looks for C header files in the right
             ;; places.
             (substitute* "man.el"
               (("\"/usr/local/include\"" line)
                (string-join
                 (list line
                       "\"~/.guix-profile/include\""
                       "\"/var/guix/profiles/system/profile/include\"")
                 " ")))
             #t))))
      (arguments
       (substitute-keyword-arguments (package-arguments emacs)
         ((#:configure-flags flags)
          `(cons* "--with-harfbuzz" ,flags))
         ((#:phases phases)
          `(modify-phases ,phases
             ;; The 'reset-gzip-timestamps phase will throw a
             ;; permission error if gzip files aren't writable then
             (add-before
                 'reset-gzip-timestamps
                 'make-compressed-files-writable
               (lambda _
                 (for-each make-file-writable
                           (find-files %output ".*\\.t?gz$"))
                 #t))
             ;; restore the dump file that Emacs installs somewhere in
             ;; libexec/ to its original state
             (add-after 'glib-or-gtk-wrap 'restore-emacs-pdmp
               (lambda* (#:key outputs target #:allow-other-keys)
                 (let* ((libexec (string-append (assoc-ref outputs "out")
                                                "/libexec"))
                        ;; each of these find-files should return one file
                        (pdmp (find-files libexec "^emacs\\.pdmp$"))
                        (pdmp-real (find-files libexec
                                               "^\\.emacs\\.pdmp-real$")))
                   (for-each (lambda (wrapper real)
                               (delete-file wrapper)
                               (rename-file real wrapper))
                             pdmp pdmp-real)
                   #t)))))))
      (inputs
       `(("jansson" ,jansson)
         ("harfbuzz" ,harfbuzz)
         ;; Emacs no longer uses ImageMagick by default
         ;; https://git.savannah.gnu.org/cgit/emacs.git/tree/etc/NEWS?h=emacs-27.0.91&id=c36c5a3dedbb2e0349be1b6c3b7567ea7b594f1c#n102
         ,@(alist-delete "imagemagick" (package-inputs emacs))))
      (native-inputs
       `(("autoconf" ,autoconf)      ; needed when building from trunk
         ,@(package-native-inputs emacs)))

      ;; TODO: consider changing `emacs' to use a more robust way of
      ;; specifying version for "EMACSLOADPATH", so as to avoid having to
      ;; duplicate native-search-paths here.
      (native-search-paths
       (list (search-path-specification
              (variable "EMACSLOADPATH")
              ;; The versioned entry is for the Emacs' builtin libraries.
              (files
               (list "share/emacs/site-lisp"
                     (string-append "share/emacs/" emacs-version "/lisp"))))
             (search-path-specification
              (variable "INFOPATH")
              (files '("share/info")))))))
